<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;

$factory->define(\App\Employee::class, function (Faker $faker) {
    return [
        'full_name' => $faker->name,
        'nick_name' => $faker->lastName,
        'age' => $faker->numberBetween(17, 35),
        'birth_date' => $faker->date('Y-m-d', 'now'),
        'address' => $faker->address,
        'mobile' => $faker->phoneNumber,
        'avatar' => $faker->imageUrl(50, 40),
        'create_by' => factory(App\User::class)
    ];
});
