<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Admin</title>

  <style>
    #activities {
      background: #f9f9f9;
      padding: 4px 8px;
      margin-bottom: 4px;
      font-family: monospace;
    }
  </style>
</head>
<body>

<h1>Admin Dashboard</h1>

<h2>Activities</h2>

<div id="messages"></div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.1.1/socket.io.js"></script>
<script>
    //var socket = io('http://localhost:3000');
    var socket = io('http://localhost:3000');
    socket.on("test-channel:App\\Events\\EmployeeEvent", function(message){
        // increase the power everytime we load test route
        console.log(message)
        // $('#power').text(parseInt($('#power').text()) + parseInt(message.data.power));
    });
</script>

</body>
</html>