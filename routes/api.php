<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::post('/register', 'UserController@register');
Route::post('/login', 'UserController@login');
Route::post('/reset-password', 'UserController@resetPassword');

Route::group(['prefix' => 'employee', 'middleware' => 'auth:api'], function(){ 
    Route::get('/', 'EmployeeController@index');
    Route::get('/{id}', 'EmployeeController@show');
    Route::post('/create', 'EmployeeController@store');
    Route::post('/create/a', 'EmployeeController@create');
    Route::post('/update/{id}', 'EmployeeController@update');
    Route::post('/delete/{id}', 'EmployeeController@destroy');
});
