<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $fillable = [
        'full_name', 'nick_name',
        'age', 'birth_date', 'address', 
        'mobile', 'avatar', 'create_by',
        'modify_by', 'deleted_at'
    ];
}
