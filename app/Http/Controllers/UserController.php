<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use DB;

class UserController extends Controller
{
    public function login(Request $request) {
        $credentials = [
            'email' => $request->email,
            'password' => $request->password
        ];
        
        if (auth()->attempt($credentials)) {
            $token = auth()->user()->createToken('tokenPassport')->accessToken;
            return response()->json([
                'error' => false,
                'message' => 'success',
                'data' => [
                    'email' => $request->email,
                    'name' => $request->name,
                    'token' => $token
                ]
            ], 200);
        } else {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
    }

    public function register(Request $request) {
        $this->validate($request, [
            'name' => 'required|min:3',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6',
        ]);
        
        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password)
        ]);
        $token = $user->createToken('tokenPassport')->accessToken;

        return response()->json([
            'error' => false,
            'message' => 'success',
            'data' => [
                'email' => $request->email,
                'name' => $request->name,
                'token' => $token
            ]
        ], 200);
    }

    public function resetPassword(Request $request) {
        $this->validate($request, [
            'id' => 'required',
            'new_password' => 'required',
            'new_password_confirmation' => 'required'
        ]);

        $checkUser = DB::table('users')->where('id', $request->id)->first();

        if (!$checkUser) {
            return response()->json([
                'error' => true,
                'message' => 'failed',
                'info' => 'User tidak ditemukan.'
            ]);
        } else {
            $newPassword = $request->new_password;
            $newPasswordConfirm = $request->new_password_confirmation;

            if ($newPassword != $newPasswordConfirm) {
                return response()->json([
                    'error' => true,
                    'message' => 'failed',
                    'info' => 'Password tidak sama.'
                ]);
            } else {
                $alterPass = DB::table('users')->where('id', $request->id)->update([
                    'password' => bcrypt($request->new_password)
                ]);

                if ($alterPass) {
                    return response()->json([
                        'error' => false,
                        'message' => 'success',
                        'info' => 'Password berhasil direset dan diupdate.',
                        'data' => [
                            'user_id' => $request->id,
                            'new_password' => $request->new_password
                        ]
                    ]);
                }
            }
        }
    }
}