<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Employee;
use Illuminate\Support\Facades\Redis;
use App\Events\EmployeeEvent;
use DB;

class EmployeeController extends Controller
{
    public function showEmployee() {
        return view('employee');
    }

    public function index(Request $request) {
        $this->validate($request, [
            'offset' => 'required',
            'limit' => 'required',
            'order' => 'required'
        ]);
        
        $order = $request->order;
        $orderKey = '';
        $orderValue = '';
        $exOrder = explode('_', $order);

        if (count($exOrder) > 2) {
            $orderKey = $exOrder[0].'_'.$exOrder[1];
            $orderValue = $exOrder[2];
        } else {
            $orderKey = $exOrder[0];
            $orderValue = $exOrder[1];
        }
        
        $getData = DB::table('employees');
        
        if ($request->search !== null) {
            $getData->where('full_name', $request->search)
                ->orWhere('nick_name', $request->search)
                ->orWhere('birth_date', $request->search)
                ->orWhere('age', $request->search);
        }

        $data = $getData
            ->skip($request->offset)
            ->take($request->limit)
            ->orderBy($orderKey, $orderValue);
        Redis::set('employee:list', $data->get());

        return response()->json([
            'error' => false,
            'message' => 'success',
            'data' => [
                'rows' => $data->get(),
                'count' => count($data->get())
            ]
        ]);
    }

    public function show($id)
    {
        $data = Employee::find($id);

        return response()->json([
            'error' => false,
            'message' => 'success',
            'data' => $data
        ]);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'full_name' => 'required',
            'nick_name' => 'required',
            'age' => 'required',
            'birth_date' => 'required',
            'address' => 'required',
            'mobile' => 'required',
            'avatar' => 'required'
        ]);
        
        $userId = auth('api')->user()->id;
        $data = [
            'full_name' => $request->full_name,
            'nick_name' => $request->nick_name,
            'age' => $request->age,
            'birth_date' => $request->birth_date,
            'address' => $request->address,
            'mobile' => $request->mobile,
            'avatar' => $request->avatar,
            'create_by' => $userId
        ];
        Employee::create($data);

        return response()->json([
            'error' => false,
            'message' => 'success',
            'info' => 'Data berhasil di tambah.',
            'data' => $data
        ]);
    }

    public function create(Request $request) {
        event(new EmployeeEvent());
        return "event fired";
    }

    public function update(Request $request, $id)
    {
        $userId = auth('api')->user()->id;
        $employee = Employee::find($id);
        $data = [
            'full_name' => $request->full_name,
            'nick_name' => $request->nick_name,
            'age' => $request->age,
            'birth_date' => $request->birth_date,
            'address' => $request->address,
            'mobile' => $request->mobile,
            'avatar' => $request->avatar,
            'modify_by' => $userId
        ];
        $employee->update(array_filter($data));

        return response()->json([
            'error' => false,
            'message' => 'success',
            'info' => 'Data berhasil di update.',
            'data' => $data
        ]);
    }

    public function destroy($id)
    {
        $employee = Employee::find($id);
        $data = [
            'deleted_at' => date('Y-m-d H:i:s')
        ];
        $employee->update($data);

        return response()->json([
            'error' => false,
            'message' => 'success',
            'info' => 'Data berhasil di hapus.',
            'data' => [
                'id' => $id
            ]
        ]);
    }
}