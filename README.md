## Usage and Install

- clone project
- cp .env.example .env
- php artisan key:generate
- composer install
- php artisan migrate
- php artisan db:seed
- php artisan passport:install
- php artisan serve